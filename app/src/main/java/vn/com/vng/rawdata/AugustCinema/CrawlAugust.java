package vn.com.vng.rawdata.AugustCinema;

import android.content.Context;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

import bsh.EvalError;
import bsh.Interpreter;
import vn.com.vng.rawdata.DataCallback;
import vn.com.vng.rawdata.Film;
import vn.com.vng.rawdata.ShowTimeContainer;
import vn.com.vng.rawdata.Showtime;

/**
 * Created by vtminh1805 on 10/30/2017.
 */

public class CrawlAugust extends Thread {
    private final String TAG = "GetFilmListCinebox";

    DataCallback<HashMap<String, ArrayList<Film>>> mCallback;
    Context mContext;
    public CrawlAugust(Context context){
        mContext = context;

    }

    public void setCallback(DataCallback<HashMap<String, ArrayList<Film>>> callback){
        mCallback = callback;
    }


    public void run() {
       crawl();
    }




    void crawl(){
        HashMap standardData = new HashMap();
        try {
            String url = "http://rapthang8.com/Home/LichChieu.aspx";
            Document doc = Jsoup.connect(url).timeout(60*1000).get();

            Element body = doc.body();

            Elements allDays = body.select(".panel-default");

            for(Element dayEl: allDays){
                String day = dayEl.select("a").get(0).text();

                ArrayList films = new ArrayList();

                Elements allFilmName = dayEl.select(".lct_tenphim");
                Elements allFilm = dayEl.select(".module_cinema_tuan");
                for (int j = 0; j< allFilmName.size(); j++){
                    ShowTimeContainer showTimeContainer = new ShowTimeContainer();
                    Elements showtimeEl = allFilm.get(j).select("li");
                    for(int k = 0; k < showtimeEl.size(); k++) {
                        Showtime showtime = new Showtime();
                        showtime.parseAugustCinema(showtimeEl.get(k).text(),day);
                        showTimeContainer.addShowtime(showtime);
                    }

                    Film film = new Film(allFilmName.get(j).text());
                    film.setShowTimeContainer(showTimeContainer);
                    films.add(film);
                }


                standardData.put(day,films);
            }

            mCallback.onResult(standardData);


        } catch (Throwable t) {
            t.printStackTrace();
        }
    }



}


