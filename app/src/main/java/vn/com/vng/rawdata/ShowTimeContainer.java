package vn.com.vng.rawdata;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by vtminh1805 on 10/30/2017.
 */

public class ShowTimeContainer {

    public static final int TYPE_BY_FILM = 0;
    public static final int TYPE_BY_CINEMA = 1;
    public static final int TYPE_OTHER = 2;


    public static final int CGV = 1;
    public static final int CINEBOX = 2;
    public static final int AUGUST = 3;

    public void setSessionList(ArrayList<Showtime> sessionList) {
        this.sessionList = sessionList;
    }

    private ArrayList<Showtime> sessionList = new ArrayList<>();
    private String mLastSync;
    private String mSkuCgv;

    private String mCinemaIdCgv;

    public int getDuration() {
        return mDuration;
    }

    public void setDuration(int mDuration) {
        this.mDuration = mDuration;
    }

    private int mDuration;


    public String getSkuCgv() {
        return mSkuCgv;
    }

    public String getCityNameCgv() {
        return mCityNameCgv;
    }

    private String mCityNameCgv;

    public ArrayList<Showtime> getShowtimeList() {
        return sessionList;
    }

    public String getLastSync() {
        return mLastSync;
    }

    public void setLastSync(String mLastUpdated) {
        this.mLastSync = mLastUpdated;
    }

    public void addShowtime(Showtime showtime) {
        sessionList.add(showtime);
    }


    public void removeShowtime(Showtime showtime) {
        sessionList.remove(showtime);
    }

    public void removeShowtime(int index) {
        sessionList.remove(index);
    }

}
