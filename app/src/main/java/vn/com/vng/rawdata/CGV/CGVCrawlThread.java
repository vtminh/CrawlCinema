package vn.com.vng.rawdata.CGV;

import android.util.Log;

import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import vn.com.vng.rawdata.Showtime;

/**
 * Created by vtminh1805 on 9/21/2017.
 */

public class CGVCrawlThread extends Thread {
    final String TAG = "CGVCrawlThread";
    String mUrl;


    ArrayList<Showtime> listSession = new ArrayList<>();


    public CGVCrawlThread(String url) {
        mUrl = url;

    }

    public void run() {

        StringBuffer buffer = new StringBuffer();
        try {
            Log.d(TAG, "Connecting to [" + mUrl + "]");
            //System.setProperty("javax.net.ssl.trustStore", "C:/Users/vtminh1805/Desktop/cgv.jsk");
            Document doc = Jsoup.connect(mUrl).get();
            Log.i(TAG, "run: doc = " + doc);

            // Get document (HTML page) title
            Element body = doc.body();

            Elements day = body.select("div.cgv-sites-schedule"); //lấy tất cả các ngày
            Element firstDay = day.get(0);

            Elements filmList = firstDay.select("div.film-list");
            //Elements filmList = body.getElementsByClass("film-list");


            for (Element film : filmList) {
                String filmName = film.select(".film-label").select("a").text(); //Tên film
                Log.d(TAG, "run: title = " + filmName);


                Elements filmRight = film.select(".film-right"); //Tất cả suất chiếu của tất cả loại vé


                Elements listFilmShowTimes = filmRight.select(".film-showtimes"); //Tất cả suất chiếu của 1 loại vé

                for (Element filmShowTimes : listFilmShowTimes) {
                    String type = filmRight.select(".film-screen").get(listFilmShowTimes.indexOf(filmShowTimes)).text(); //Loai vé 2D, 3D...

                    Elements productsGridMovie = filmShowTimes.select(".products-grid-movie");
                    Elements items = productsGridMovie.select(".item");
                    for (Element item : items) {
                        String startTime = item.select("span").get(0).text();
                        String availableSeat = item.select("span").get(1).text(); //100 ghế trống

                        int indexSpace = availableSeat.indexOf(" ");  //index của khoảng trắng đầu tiên
                        int numberSeat = Integer.parseInt(availableSeat.substring(0, indexSpace));// lấy đc só 100
                        //Log.i(TAG, "run: time = "  + item.select("span").get(0).text() );
                        //Log.i(TAG, "run: available seat = "  + item.select("span").get(1).text() );

                        //Showtime session = new Showtime(filmName, startTime, type, numberSeat);

                        //listSession.add(session);
                    }
                }
            }
            //Log.i(TAG, "run: listSesion = " + listSession);

            Gson gson = new Gson();
            String json = gson.toJson(listSession);

            int maxLogSize = 1000;
            for(int i = 0; i <= json.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i+1) * maxLogSize;
                end = end > json.length() ? json.length() : end;
                Log.v(TAG, json.substring(start, end));
            }


        } catch (Throwable t) {
            t.printStackTrace();
        }
    }


}

