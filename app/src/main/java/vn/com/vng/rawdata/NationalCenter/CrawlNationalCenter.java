package vn.com.vng.rawdata.NationalCenter;

import android.content.Context;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;

import vn.com.vng.rawdata.DataCallback;
import vn.com.vng.rawdata.Film;
import vn.com.vng.rawdata.ShowTimeContainer;
import vn.com.vng.rawdata.Showtime;

/**
 * Created by vtminh1805 on 10/31/2017.
 * Rạp theo lấy suất chiếu theo từng ngày được, ko cần lấy hết
 */

public class CrawlNationalCenter extends Thread {
    private final String TAG = "GetFilmListCinebox";
    private String mUrl;
    String mDate;


    DataCallback<HashMap<String, ArrayList<Film>>> mCallback;
    Context mContext;

    public CrawlNationalCenter(Context context, String date) {
        mContext = context;
        mDate = date;
        String basicUrl = "https://chieuphimquocgia.com.vn/Film/BoxLichChieuPhimShowAll2?projectDate=" + mDate + "+12:00:00+SA";
        try {
            mUrl = URLDecoder.decode(basicUrl, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    public void setCallback(DataCallback<HashMap<String, ArrayList<Film>>> callback) {
        mCallback = callback;
    }


    public void run() {
        crawl();

    }


    void crawl(){
        HashMap standardData = new HashMap(); //key: ngày, value: list film
        try {
            String url = "https://chieuphimquocgia.com.vn/Film/BoxLichChieuPhimShowAll2?projectDate=" + mDate + "+12:00:00+SA";

            Log.d(TAG, "Connecting to [" + url + "]");
            //System.setProperty("javax.net.ssl.trustStore", "C:/Users/vtminh1805/Desktop/cgv.jsk");
            Document doc = Jsoup.connect(url).timeout(60 * 1000).get();
            //Log.i(TAG, "run: doc = " + doc);

            // Get document (HTML page) title
            Element body = doc.body();

            ///start parse
            ArrayList films = new ArrayList();
            Elements allFilm = body.select(".content_box_showtime > table > tbody > tr"); //lấy tất cả các film

            for (Element elFilm : allFilm) {
                Elements infoShowtime = elFilm.select(".movie_showtime").get(0).select("td");
                //infoShowtime Có 3 thẻ td:
                //Thẻ 1: 4d/3d/2d => thẻ đầu
                //Thẻ 2: 16+ 18+ (ko cần quan tâm thẻ này)
                //thẻ 3: Tên film => thẻ cuối


                Element firstTag = infoShowtime.get(0);
                Element lastTag = infoShowtime.get(infoShowtime.size() - 1);

                String version = firstTag.select("span").get(0).className();
                String filmName = lastTag.select("b").get(0).text();

                Film film = new Film(filmName);

                ShowTimeContainer showTimeContainer = new ShowTimeContainer();
                Elements elSessions = elFilm.select(".screen_showtime").get(0).select("a");
                for (int i = 0; i < elSessions.size(); i++) {
                    Showtime showtime = new Showtime();
                    showtime.parseNationCenter(elSessions.get(i).text(), version);
                    showTimeContainer.addShowtime(showtime);
                }
                film.setShowTimeContainer(showTimeContainer);
                films.add(film);
            }

            standardData.put(mDate, films);

            mCallback.onResult(standardData);


        } catch (Throwable t) {
            t.printStackTrace();
        }
    }


}



