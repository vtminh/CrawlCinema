package vn.com.vng.rawdata;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by vtminh1805 on 11/1/2017.
 */

public class VersionManager extends AsyncTask<String, String, String> {

    public interface OnDownloadListener{
        void onStart();
        void onFinish();
    }

    String TAG  = "DownloadBundleService";

    boolean isOccurError = false;
    private Context mContext;
    OnDownloadListener listener;

    String mFileScript;

    public VersionManager(Context context, String fileScript){
        mContext = context.getApplicationContext();
        mFileScript = fileScript;
    }
    /**
     * Before starting background thread
     * */

    public void setListener(OnDownloadListener listener){
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        System.out.println("Starting download");
        if(listener!=null)
            listener.onStart();
    }

    /**
     * Downloading file in background thread
     * */

    @Override
    protected String doInBackground(String... paramses) {
        int count;

        String timeStampSer = "";

        try {

            ContextWrapper cw = new ContextWrapper(mContext);

            File directory = cw.getDir("Crawl", Context.MODE_PRIVATE);
            File mypath = new File(directory, mFileScript + ".bsh");

            System.out.println("Downloading");


            URL url = new URL("https://phim-123.firebaseapp.com/code-crawl/" + mFileScript +".bsh" );

            URLConnection conection = url.openConnection();

            Map<String, List<String>> map = conection.getHeaderFields();
            timeStampSer = map.get("Last-Modified") != null ? map.get("Last-Modified").get(0): "";



            Log.i(TAG, "doInBackground: start download file bundle");

            conection.connect();
            // Getting file length
            int lenghtOfFile = conection.getContentLength();

            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream to write file

            OutputStream output = new FileOutputStream(mypath);
            byte data[] = new byte[1024];

            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

            if (total == lenghtOfFile) {

                SharedPreferences pre = mContext.getSharedPreferences("bundle_js", MODE_PRIVATE);
                SharedPreferences.Editor edit = pre.edit();
                edit.putString("index.android.bundle", timeStampSer);
                edit.apply();


            }else{
                isOccurError = true; //Nếu download bị lỗi thì ko load react
            }



        } catch (Exception e) {
            e.printStackTrace();
        }

        return timeStampSer;
    }

    /**
     * After completing background task
     * **/
    @Override
    protected void onPostExecute(String file_url) {
        Log.i(TAG, "doInBackground: complete.");
        if(listener!=null && !isOccurError)
            listener.onFinish();
    }
}

