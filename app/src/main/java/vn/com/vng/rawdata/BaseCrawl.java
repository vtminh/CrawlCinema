package vn.com.vng.rawdata;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

import bsh.EvalError;
import bsh.Interpreter;

/**
 * Created by vtminh1805 on 11/3/2017.
 */

public class BaseCrawl extends Thread {
    DataCallback<HashMap<String, ArrayList<Film>>> mCallback;
    Context mContext;
    String mCinemaName;
    String mDate;

    public BaseCrawl(Context context, String cinemaName, String date) {
        mContext = context;
        mCinemaName = cinemaName;
        mDate = date;

    }

    public void setCallback(DataCallback<HashMap<String, ArrayList<Film>>> callback) {
        mCallback = callback;
    }

    public void run() {

        Interpreter i = new Interpreter();

        ContextWrapper cw = new ContextWrapper(mContext);
        File directory = cw.getDir("Crawl", Context.MODE_PRIVATE);
        File file = new File(directory, mCinemaName + ".bsh");

        try {
            // Creates a Reader which has read the file
            //Reader reader = new FileReader(file);

            //eval() is used to either run a String as a script or the reader
            //i.eval(reader);
            //source() is used to run a script from the path name
            i.set("mContext", mContext);
            i.set("mCallback", mCallback);
            i.set("mDate", mDate);
            i.set("baseCrawl", BaseCrawl.this);
            i.source(file.getAbsolutePath());


            //EvalError is from the Beanshell Interpreter, and the other two are the standard IO operation exceptions
        } catch (EvalError evalError) {
            evalError.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
