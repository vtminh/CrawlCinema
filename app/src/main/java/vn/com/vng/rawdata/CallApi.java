package vn.com.vng.rawdata;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by vtminh1805 on 10/30/2017.
 */

public class CallApi extends Thread {
    final int SUCCESS = 1;
    final int MESSAGE = 0;
    final int FAIL = -1;

    public static final int TIME_OUT_WAIT = 15000;
    String mParams;
    String mURL;
    DataCallback<String> mCallback;
    boolean isGet = true;

    Context mContext;

    public CallApi(Context context, String url, String params, DataCallback<String> callback) {
        mURL = url;
        mParams = params;
        mCallback = callback;
        isGet = false;

        mContext = context;
    }

    CallApi(Context context, String url, DataCallback<String> callback) {
        mURL = url;
        isGet = true;
        mCallback = callback;
        mContext = context;
    }

    public void run() {
        String response = requestHttpPOST(mURL, mParams, isGet);

        String result = null;
        try {
            if (StringUtil.isNotNullString(response)) {
                JSONObject jData = new JSONObject(response);
                mCallback.onResult(jData.optString("d"));

            } else {
                Handler mainThread = new Handler(mContext.getMainLooper());
                mainThread.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext, "Không có kết nối.", Toast.LENGTH_SHORT).show();
                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    /**
     * Ver2 sẽ truyền userId vào header
     */
    public static String requestHttpPOST(String url, String params, boolean isGet) {
        String response = null;
        try {
            URL uUrl = new URL(url);

            HttpURLConnection conn = (HttpURLConnection) uUrl.openConnection();
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Cookie", "AspxAutoDetectCookieSupport=1; ASP.NET_SessionId=1d5mmydxhixot3e1wju0to20");
            if(!isGet)
                conn.setRequestMethod("POST");
            conn.setReadTimeout(TIME_OUT_WAIT);
            conn.setConnectTimeout(TIME_OUT_WAIT);
            conn.setDoInput(true);

            if (!isGet)
                conn.setDoOutput(true);

            if (!isGet) {
                DataOutputStream printout = new DataOutputStream(conn.getOutputStream());
                printout.write(params.getBytes("UTF8"));
                printout.flush();
                printout.close();
            }

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                InputStream streamContent = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(streamContent));
                StringBuilder str = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
                response = str.toString();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;

    }


}
