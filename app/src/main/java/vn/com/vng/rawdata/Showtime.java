package vn.com.vng.rawdata;

import org.json.JSONObject;

/**
 * Created by vtminh1805 on 9/21/2017.
 */


/**
 * Created by vtminh1805 on 10/10/2017.
 */

public class Showtime {
    private long mID;
    private long mRoomId;
    private String mSessionTime;
    private int mStatusId;
    private String mSessionUrl;
    private String mRoomName;
    private String mDescVi;
    private String mDescEn;
    private  int isVoice;

    public String getDate() {
        return mDate;
    }

    private String mDate;


    public void setMinPrice(int mMinPrice) {
        this.mMinPrice = mMinPrice;
    }

    public int getMinPrice() {
        return mMinPrice;
    }

    private int mMinPrice = 0;


    public Showtime(){}

    public void parseAugustCinema(String sessionTime, String date){
        mSessionTime = sessionTime;
        mDate = date;
    }

    public void parseCinebox(JSONObject jsonObject){
        mSessionTime = jsonObject.optString("TIME");
        mDate = jsonObject.optString("ShowDateString").substring(5);
    }


    public void parseNationCenter(String sessionTime, String desc){
        if(desc.equals("icon_4d")){
            mDescVi = "4D";
            mDescEn = "4D";
        }
        else if(desc.equals("icon_3d")){
            mDescVi = "3D";
            mDescEn = "3D";
        }
        else {
            mDescVi = "2D";
            mDescEn = "2D";
        }
        mSessionTime = sessionTime;

    }
//    public Showtime(JSONObject jObject) {
//        try {
//            mID = jObject.getLong("session_id");
//            mRoomId = jObject.optLong("room_id");
//            mSessionTime = jObject.getString("session_time");
//            mStatusId = jObject.optInt("status_id");
//            mSessionUrl = jObject.optString("session_link");
//            mRoomName = jObject.optString("room_name");
//            mDescVi = jObject.optString("version_desc_vi");
//            mDescEn = jObject.optString("version_desc_en");
//            isVoice = jObject.optInt("is_voice");
//            mMinPrice = jObject.optInt("min_price");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//
//    //Hack
//    public Showtime(String hackCinemaId, JSONObject jObject, String currentDate) {
//        try {
//            mSessionTime = currentDate + " " + jObject.getString("time") + ":00";
//
//            String versionFull = jObject.getString("sub_type");
//            if(versionFull.contains("Lồng Tiếng Việt")){
//                mDescVi = jObject.getString("code") + "-Lồng tiếng";
//                mDescEn = jObject.getString("code") + "-Dub";
//            }
//            else{
//                mDescVi = jObject.getString("code") + "-Phụ đề";
//                mDescEn = jObject.getString("code") + "-Sub";
//            }
//
//            String idSessionCgv = jObject.getString("id");
//
//            mStatusId = 1;
//            mSessionUrl = "https://www.cgv.vn/default/cinox/sales/seats/site/" + hackCinemaId + "/id/" + idSessionCgv;
//            Log.i("TAG", "Session: mSessionUrl = " + mSessionUrl);
//
//
//
//        } catch (JSONException e) {
//            Log.e("Session", "Parse session content error " + e.getMessage());
//        }
//    }


    public String getDescVn(){
        return mDescVi;
    }

    public String getDescEn(){
        return mDescEn;
    }


    public long getId() {
        return mID;
    }

    public long getRoomId() {
        return mRoomId;
    }


    public String getSessionTime() {
        return mSessionTime;
    }


    public int getStatusId() {
        return mStatusId;
    }


    public String getSessionUrl() {
        return mSessionUrl;
    }

//    public String getFilmPoster() {
//        return mFilmPoster;
//    }

    public String getRoomName() {
        return mRoomName;
    }

}
