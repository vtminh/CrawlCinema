package vn.com.vng.rawdata;

/**
 * Created by vtminh1805 on 10/30/2017.
 */


public interface DataCallback<T> {
    void onResult(T result);
}