package vn.com.vng.rawdata;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import bsh.Interpreter;
import vn.com.vng.rawdata.AugustCinema.CrawlAugust;
import vn.com.vng.rawdata.Cinebox.CrawlCinebox;


public class MainActivity extends AppCompatActivity {

    String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        runScript("NationalCenter","3/11/2017");
//        runScript("August",""); //ko cần truyền ngày vì web của rạp này lấy tất cả các ngày trong tuần
//        runScript("Cinebox","3/11/2017");
//

        runScript("Cinebox", "");



//        CrawlCinebox crawlCinebox = new CrawlCinebox(this);
//        crawlCinebox.setCallback(new DataCallback<HashMap<String, ArrayList<Film>>>() {
//            @Override
//            public void onResult(HashMap<String, ArrayList<Film>> result) {
//                Log.i(TAG, "onResult: " + result);
//            }
//        });
//        crawlCinebox.start();


//

    }

    private void runScript(final String fileScript, final String date){
        VersionManager versionManager = new VersionManager(this,fileScript);
        versionManager.execute();
        versionManager.setListener(new VersionManager.OnDownloadListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {

                BaseCrawl baseCrawl = new BaseCrawl(MainActivity.this,fileScript,date) ;
                baseCrawl.setCallback(new DataCallback<HashMap<String, ArrayList<Film>>>() {
                    @Override
                    public void onResult(HashMap<String, ArrayList<Film>> result) {
                        Log.i(TAG, "onResult: " + result);
                    }
                });
                baseCrawl.start();
            }
        });
    }

}
