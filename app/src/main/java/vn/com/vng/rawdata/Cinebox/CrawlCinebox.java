package vn.com.vng.rawdata.Cinebox;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import vn.com.vng.rawdata.CallApi;
import vn.com.vng.rawdata.DataCallback;
import vn.com.vng.rawdata.Film;
import vn.com.vng.rawdata.ShowTimeContainer;
import vn.com.vng.rawdata.Showtime;

/**
 * Created by vtminh1805 on 10/27/2017.
 */

public class CrawlCinebox extends Thread {
    private final String TAG = "GetFilmListCinebox";
    private String mUrl = "http://cinebox.vn/lich-chieu";



    DataCallback<HashMap<String, ArrayList<Film>>> mCallback;
    Context mContext;

    public CrawlCinebox(Context context){
        mContext = context;
    }

    public void setCallback(DataCallback<HashMap<String, ArrayList<Film>>> callback){
        mCallback = callback;
    }


    public void run() {
//        GetShowtimeCineboxThread getShowtimeCineboxThread = new GetShowtimeCineboxThread(mContext, getAllFilmNames());
//        getShowtimeCineboxThread.setCallback(mCallback);
//        getShowtimeCineboxThread.start();

        crawl();

    }

    private ArrayList<String>  getAllFilmNames(){
        ArrayList<String> allFilmNames = new ArrayList<>();
        try {

            Log.d(TAG, "Connecting to [" + mUrl + "]");
            //System.setProperty("javax.net.ssl.trustStore", "C:/Users/vtminh1805/Desktop/cgv.jsk");
            Document doc = Jsoup.connect(mUrl).timeout(60*1000).get();
            //Log.i(TAG, "run: doc = " + doc);

            // Get document (HTML page) title
            Element body = doc.body();

            Elements allFilms = body.select(".showtime-id"); //lấy tất cả các film

            for(Element film: allFilms){
                allFilmNames.add( film.select("div").get(0).id());
                Log.i(TAG, "run: film name = " + film.select("div").get(0).id());
            }



        } catch (Throwable t) {
            t.printStackTrace();
        }

        return allFilmNames;
    }


    //Ở cinebox có 2 bước:
    //B1: Lấy tất cả film name
    //B2: lấy data từ film name đó
    void crawl(){
        String mUrlShowtime = "http://cinebox.vn/Cine_Movie_ShowTimes_New.aspx/LoadLichChieu";

        ExecutorService mThreadPool =  Executors.newFixedThreadPool(4);

        final ArrayList tempDays = new ArrayList(); //dùng d? luu s? ngày (R?p cinebox thu?ng là 2 ngày)

        final ArrayList allFilms = new ArrayList(); //Ch?a t?t c? các su?t chi?u c?a t?t c? ngày, t?t c? r?p

        ArrayList allFilmNames = new ArrayList();
        try {

            //System.setProperty("javax.net.ssl.trustStore", "C:/Users/vtminh1805/Desktop/cgv.jsk");
            Document doc = Jsoup.connect("http://cinebox.vn/lich-chieu").timeout(60*1000).get();
            //Log.i(TAG, "run: doc = " + doc);

            // Get document (HTML page) title
            Element body = doc.body();

            final Elements allFilmsEl = body.select(".showtime-id"); //l?y t?t c? các film

            for(Element film: allFilmsEl){
                allFilmNames.add( film.select("div").get(0).id());
                //Log.i("TAG", "run: film name = " + film.select("div").get(0).id());
            }


            for (Object object : allFilmNames) {
                final String filmName = (String) object;
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("NameRewrite", filmName);
                    //jsonObject.put("Cookie", "AspxAutoDetectCookieSupport=1; ASP.NET_SessionId=1d5mmydxhixot3e1wju0to20");
                    CallApi callApiCinebox = new CallApi(mContext, mUrlShowtime,
                            jsonObject.toString(), new DataCallback() {

                        public void onResult(Object objectResult) {

                            ArrayList sessionList = new ArrayList();
                            //baseCrawl.parseCinebox(sessionList, objectResult, tempDays);

                            String result = (String) objectResult;

                            try {


                                JSONArray jsonArray = new JSONArray(result);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {

                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        Showtime showtime = new Showtime();
                                        showtime.parseCinebox(jsonObject);
                                        sessionList.add(showtime);


                                        if (!tempDays.contains(showtime.getDate())) {
                                            tempDays.add(showtime.getDate());
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            ShowTimeContainer showTimeContainer = new ShowTimeContainer();
                            showTimeContainer.setSessionList(sessionList);
                            Film film = new Film(filmName);
                            film.setShowTimeContainer(showTimeContainer);


                            allFilms.add(film);

                        }
                    });
                    mThreadPool.execute(callApiCinebox);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            mThreadPool.shutdown();
            try {
                mThreadPool.awaitTermination(1, TimeUnit.MINUTES);

                HashMap standardData = new HashMap(); //key: ngày, value: list film
                for (Object objectDay : tempDays) {
                    String day = (String) objectDay;
                    ArrayList filmsByDay = new ArrayList(allFilms.size());
                    for (Object objectFilm : allFilms) {
                        Film film = (Film) objectFilm;
                        Film filterFilm = new Film(film.getName()); //Film dã l?c su?t chi?u
                        for (Iterator iterator = film.getShowTimeContainer().getShowtimeList().iterator(); iterator.hasNext(); ) {
                            Showtime showtime = (Showtime) iterator.next();
                            if (showtime.getDate().equals(day)) {
                                // Remove the current element from the iterator and the list.
                                //iterator.remove();

                                filterFilm.getShowTimeContainer().addShowtime(showtime);
                                //filmsByDay.add(filterFilm);
                            }
                        }

                        filmsByDay.add(filterFilm);
                    }

                    standardData.put(day, filmsByDay);


                }

                mCallback.onResult(standardData);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } catch (Throwable t) {
            t.printStackTrace();
        }


    }



}


