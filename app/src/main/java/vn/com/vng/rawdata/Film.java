package vn.com.vng.rawdata;

/**
 * Created by vtminh1805 on 10/31/2017.
 */

import android.database.Cursor;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

public class Film implements Serializable {
    /**
     * Film Age 13
     */
    public static final int AGE_13 = 13;

    /**
     * Film Age 16
     */
    public static final int AGE_16 = 16;

    /**
     * Film Age 18
     */
    public static final int AGE_18 = 18;

    public static final String FILM_ID = "film_id";
    public static final String FILM_NAME = "film_name";
    public static final String IMAGE = "image";

    private static String LOG_TAG = "FilmInfo";

    /**
     * Status của tab not film
     */
    public static final int STATUS_NOT_FILM = 0;

    /**
     * Status của phim đang chiếu
     */
    public static final int STATUS_SHOWING = 2;

    /**
     * Status của phim sắp chiếu
     */
    public static final int STATUS_COMING = 1;

    /**
     * Type id xác định là phim Bom Tấn
     */
    public static final int TYPE_HOT = 2;

    /**
     * Id của phim
     */
    private long mID;

    /**
     * Tên phim
     */
    private String mName;
    private String mVnName;
    private String mEngName;

    /**
     * Link poster
     */
    private String mPosterURL;

    private String mPosterFull;

    private String mPosterLandscape;

    /**
     * Đuôi đường dẫn chi tiết
     */
    private String mFilmUrl;

    private ArrayList<String> mTrailerUrl;

    /**
     * Thời lượng
     */
    private int mDuration;

    /**
     * Status id
     */
    private int mStatusId;

    /**
     * Giới thiệu
     */
    private String mDesccription;

    /**
     * Phim yêu thích
     */
    private boolean mIsFavorite;

    /**
     * Ngày khởi chiếu
     */
    private String mPublishedDateStr;

    private long mPublishedDateMillsec;

    /**
     * Thể loại phim 2D, digital,3D...
     */
    private String mVersion;

    /**
     * Tổng số lượt đánh giá
     */
    private int mTotalRatings;
    private double mUserRatings;

    /**
     * Điểm đánh giá trung bình
     */
    private double mIMDb;

    /**
     * List Image Thumbnail
     */
    private ArrayList<String> mThumbnailUrls = new ArrayList<String>();

    /**
     * List Image View
     */
    private ArrayList<String> mImageViewUrls = new ArrayList<String>();


    /**
     * id media
     */
    private String mMediaId;

    /**
     * id zing
     */
    private String mZingTVId;

    /**
     * Thứ tự sắp xếp theo server qui định
     */
    private int mOrder;

    /**
     * Phim này có được đăng ký thông báo bán vé hay không
     */
    private boolean mNotifyComingSessions = false;


    /**
     * Các giá trị để hiển thị phim mới hoặc phim bom tấn
     */
    private int mType;
    private boolean mIsNew;

    /**
     * Danh sách các rạp có chiếu phim này
     */
    private String mLocationIdList;

    private int mHasSession;
    private int mFilmAge;

    private JSONArray mFilmsRelated;
    private String mPGRating;
    private int mHasDetail;

    public ShowTimeContainer getShowTimeContainer() {
        return showTimeContainer;
    }

    public void setShowTimeContainer(ShowTimeContainer showTimeContainer) {
        this.showTimeContainer = showTimeContainer;
    }

    private ShowTimeContainer showTimeContainer = new ShowTimeContainer();

    public Film(String filmName){
        mName = filmName;

    }

    //Clone
    public Film(Film film){
        mName = film.getName();
        showTimeContainer = film.getShowTimeContainer();

    }

//    @Override
//    public Film clone(){
//        try {
//            return (Film) super.clone();
//        } catch (CloneNotSupportedException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }

    /**
     * Dùng cho màn hình lịch chiếu theo rạp
     */
    public Film(int filmId, String filmName, String poster, String pgRating, int duration, String nameVn, String nameEn, int filmAge ){
        mID = filmId;
        mName = filmName;
        mPosterURL = poster;
        mPGRating = pgRating;
        mDuration = duration;
        mVnName = nameVn;
        mEngName = nameEn;
        mFilmAge = filmAge;
    }

    public int hasDetail() {
        return mHasDetail;
    }

    public String getPGRating() {
        return mPGRating;
    }



    public long getId() {
        return mID;
    }

    public String getName() {
        if (mName == null) {
            mName = "";
        }
        return mName;
    }

    public String getPosterFull() {
        return mPosterFull;
    }

    public String getPosterLandscape() {
        return mPosterLandscape;
    }

    public String getPosterURL() {
        return mPosterURL;
    }

    public String getFilmUrl() {
        return mFilmUrl;
    }

    public int getDuration() {
        return mDuration;
    }

    public int getStatus() {
        return mStatusId;
    }

    public String getDescription() {
        return mDesccription;
    }

    public String getPublishedDate() {
        return mPublishedDateStr;
    }

    public long getPublishedDateMillisecond() {
        return mPublishedDateMillsec;
    }

    public String getVersion() {
        return mVersion;
    }

    public void setVersion(String version) {
        mVersion = version;
    }

    public int getTotalRatings() {
        return mTotalRatings;
    }

    public void setTotalRatings(Integer mTotalRatings) {
        this.mTotalRatings = mTotalRatings;
    }

    public double getIMDb() {
        return mIMDb;
    }


    public ArrayList<String> getThumbnailUrls() {
        return mThumbnailUrls;
    }

    public ArrayList<String> getImageViewUrls() {
        return mImageViewUrls;
    }

    public void setTrailerUrl(ArrayList<String> trailers) {
        mTrailerUrl = trailers;
    }

    public ArrayList<String> getTrailerUrl() {
        return mTrailerUrl;
    }

    public String getMediaId() {
        return mMediaId;
    }

    public String getZingTVId() {
        return mZingTVId;
    }

    public boolean isFavorite() {
        return mIsFavorite;
    }

    public void setFavorite(boolean isFavorite) {
        mIsFavorite = isFavorite;
    }

    public int getOrder() {
        return mOrder;
    }

    public void setOrder(int order) {
        mOrder = order;
    }

    public boolean isNotifyComingSessions() {
        return mNotifyComingSessions;
    }

    public void setNotifyComingSessions(boolean notifyComingSessions) {
        mNotifyComingSessions = notifyComingSessions;
    }

    public int getType() {
        return mType;
    }

    public boolean isNew() {
        return mIsNew;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        else if (!(obj instanceof Film)) {
            return false;
        }

        return ((Film) obj).getId() == mID;
    }

    /**
     * Dùng để sort theo thứ tự server trả về
     *
     * @author Tran Vu Tat Binh (tranvutatbinh@gmail.com)
     */
    public static class FilmServerOrderComparator implements Comparator<Film> {

        @Override
        public int compare(Film film1, Film film2) {
            if (film1.getOrder() < film2.getOrder()) {
                return -1;
            } else if (film1.getOrder() > film2.getOrder()) {
                return 1;
            } else {
                return 0;
            }
        }

    }

    public String getLocationIdList() {
        if (mLocationIdList == null) {
            mLocationIdList = "";
        }
        return mLocationIdList;
    }


    public String getVnName() {
        if (mVnName == null) {
            mVnName = "";
        }
        return mVnName;
    }

    public String getEngName() {
        if (mEngName == null) {
            mEngName = "";
        }
        return mEngName;
    }

    public double getUserRatings() {
        return mUserRatings;
    }

    public void setUserRatings(double userRating) {
        mUserRatings = userRating;
    }

    public int getHasSession() {
        return mHasSession;
    }

    public int getFilmAge() {
        return mFilmAge;
    }

    public JSONArray getFilmsRelated() {
        return mFilmsRelated;
    }



}
