package vn.com.vng.rawdata.Cinebox;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


import vn.com.vng.rawdata.DataCallback;
import vn.com.vng.rawdata.CallApi;
import vn.com.vng.rawdata.Film;
import vn.com.vng.rawdata.ShowTimeContainer;
import vn.com.vng.rawdata.Showtime;

/**
 * Created by vtminh1805 on 10/30/2017.
 */

public class GetShowtimeCineboxThread extends Thread {

    String TAG = "GetShowtimeCinebox";
    ArrayList<String> mFilmNameList;
    Context mContext;
    String url = "http://cinebox.vn/Cine_Movie_ShowTimes_New.aspx/LoadLichChieu";
    DataCallback<HashMap<String, ArrayList<Film>>> mCallback;

    private ExecutorService mThreadPool;
    private final int MAX_THREAD = 4;

    ArrayList<String> tempDays = new ArrayList<>(); //dùng để lưu số ngày (Rạp cinebox thường là 2 ngày)

    GetShowtimeCineboxThread(Context context, ArrayList<String> filmNameList) {
        mFilmNameList = filmNameList;
        mContext = context;
        mThreadPool = Executors.newFixedThreadPool(MAX_THREAD);

    }

    public void setCallback(DataCallback<HashMap<String, ArrayList<Film>>> dataCallback) {
        mCallback = dataCallback;
    }

    public void run() {

        final ArrayList<Film> allFilms = new ArrayList<>(); //Chứa tất cả các suất chiếu của tất cả ngày, tất cả rạp

        for (final String filmName : mFilmNameList) {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("NameRewrite", filmName);
                //jsonObject.put("Cookie", "AspxAutoDetectCookieSupport=1; ASP.NET_SessionId=1d5mmydxhixot3e1wju0to20");
                CallApi callApiCinebox = new CallApi(mContext, url,
                        jsonObject.toString(), new DataCallback<String>() {
                    @Override
                    public void onResult(final String result) {
                        //mCallback.onResult(parseCinebox(result));
                        allFilms.add(parseCinebox(filmName, result));

                    }
                });
                mThreadPool.execute(callApiCinebox);
                ;
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        mThreadPool.shutdown();
        try {
            mThreadPool.awaitTermination(1, TimeUnit.MINUTES);

            convertToStandardData(allFilms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    /**
     * Parse theo từng Film
     *
     * @param filmName: tên film
     * @param data:     suất chiếu của film
     * @return Film
     */
    private Film parseCinebox(String filmName, String data) {
        ArrayList<Showtime> sessionList = new ArrayList<>();

        try {

            JSONArray jsonArray = new JSONArray(data);


            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Showtime showtime = new Showtime();
                    showtime.parseCinebox(jsonObject);
                    sessionList.add(showtime);


                    if (!tempDays.contains(showtime.getDate())) {
                        tempDays.add(showtime.getDate());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        ShowTimeContainer showTimeContainer = new ShowTimeContainer();
        showTimeContainer.setSessionList(sessionList);
        Film film = new Film(filmName);
        film.setShowTimeContainer(showTimeContainer);

        return film;

    }

    private void convertToStandardData(ArrayList<Film> allFilms) {
        HashMap<String, ArrayList<Film>> standardData = new HashMap<>(); //key: ngày, value: list film
        for (String day : tempDays) {
            ArrayList<Film> filmsByDay = new ArrayList<Film>(allFilms.size());
            for (Film film : allFilms) {
                Film filterFilm = new Film(film.getName()); //Film đã lọc suất chiếu
                for (Iterator<Showtime> iterator = film.getShowTimeContainer().getShowtimeList().iterator(); iterator.hasNext(); ) {
                    Showtime showtime = iterator.next();
                    if (showtime.getDate().equals(day)) {
                        // Remove the current element from the iterator and the list.
                        //iterator.remove();

                        filterFilm.getShowTimeContainer().addShowtime(showtime);
                        //filmsByDay.add(filterFilm);
                    }
                }

                filmsByDay.add(filterFilm);
            }

            standardData.put(day, filmsByDay);


        }

        mCallback.onResult(standardData);
    }

    private void crawl(){
        final ArrayList allFilms = new ArrayList(); //Chứa tất cả các suất chiếu của tất cả ngày, tất cả rạp

        for (final String filmName : mFilmNameList) {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("NameRewrite", filmName);
                //jsonObject.put("Cookie", "AspxAutoDetectCookieSupport=1; ASP.NET_SessionId=1d5mmydxhixot3e1wju0to20");
                CallApi callApiCinebox = new CallApi(mContext, url,
                        jsonObject.toString(), new DataCallback<String>() {
                    @Override
                    public void onResult(final String result) {
                        //mCallback.onResult(parseCinebox(result));
                        allFilms.add(parseCinebox(filmName, result));

                    }
                });
                mThreadPool.execute(callApiCinebox);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        mThreadPool.shutdown();
        try {
            mThreadPool.awaitTermination(1, TimeUnit.MINUTES);

            HashMap standardData = new HashMap(); //key: ngày, value: list film
            for (String day : tempDays) {
                ArrayList filmsByDay = new ArrayList(allFilms.size());
                for (Object object : allFilms) {
                    Film film = (Film) object;
                    Film filterFilm = new Film(film.getName()); //Film đã lọc suất chiếu
                    for (Iterator<Showtime> iterator = film.getShowTimeContainer().getShowtimeList().iterator(); iterator.hasNext(); ) {
                        Showtime showtime = iterator.next();
                        if (showtime.getDate().equals(day)) {
                            // Remove the current element from the iterator and the list.
                            //iterator.remove();

                            filterFilm.getShowTimeContainer().addShowtime(showtime);
                            //filmsByDay.add(filterFilm);
                        }
                    }

                    filmsByDay.add(filterFilm);
                }

                standardData.put(day, filmsByDay);


            }

            mCallback.onResult(standardData);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
